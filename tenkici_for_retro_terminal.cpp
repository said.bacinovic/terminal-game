#include<iostream>
#include<unistd.h>
#include<istream>
#include<unistd.h>
#include<termios.h>
#include<stdio.h>
#include<thread>
#include<mutex>
#include<chrono>
#include<vector>
#include<time.h>

using namespace std;

#define heightMatrix 14
#define widthMatrix 40

struct Enemy{
  int x;
  int y;
};
struct Shot{
  int x;
  int y;
};

//global variables
mutex inputMutex;

char matrix[heightMatrix][widthMatrix];
vector<Enemy> enemies;
vector<Shot> shots;
Enemy goodGuy;
bool gameOver = false;
int level = 1;
int kills = 0;


//function declarations
int getch();
void setUp();
void setMatrix();
void generateEnemies();
void drawMatrix();
void input();
void inputLoop();
void update();
void updateEnemies();
void updateShot();



int main(int argc, char *argv[])
{
  setUp();
  generateEnemies();
  thread inputThread(inputLoop);
  while(!gameOver){
    drawMatrix();
    update();
    this_thread::sleep_for(chrono::milliseconds(20));
  }
  
  cout<<"Game Over\nNumber of kills: "<<kills<<"Level reached: "<<level<<endl;
  return 0;
}

int getch()
{
  struct termios t;
  int c;
  tcgetattr(0,&t);
  t.c_lflag &=~ECHO+~ICANON;
  tcsetattr(0,TCSANOW,&t);
  fflush(stdout);
  c=getchar();
  t.c_lflag |=ICANON+ECHO;
  tcsetattr(0, TCSANOW, &t);
  return c;
}

void setUp(){
  srand(time(NULL));

  goodGuy.y=heightMatrix-2;
  goodGuy.x=widthMatrix/2;
}

void generateEnemies(){
  while(enemies.size() < 3+level){
    Enemy newEnemy;
    newEnemy.x=rand()%(widthMatrix-2)+1;
    newEnemy.y=0;
    enemies.push_back(newEnemy);
  }
}

void drawMatrix(){
  std::system("clear");
  for(int i=0; i<heightMatrix; i++)
    for(int j=0; j<widthMatrix; j++)
      matrix[i][j]=' ';
  
  for(int i=0; i<widthMatrix; i++)
    matrix[0][i]='*';
  for(int i=0; i<widthMatrix; i++)
    matrix[heightMatrix-1][i]='*';
  for(int i=0; i<heightMatrix; i++)
    matrix[i][0]='*';
  for(int i=0; i<heightMatrix; i++)
    matrix[i][widthMatrix-1]='*';

  for(auto i:enemies){
    if(i.x-1>0 && i.x-1<widthMatrix && i.y-1<heightMatrix && i.y-1>0)
      matrix[i.y-1][i.x-1]='*';
    if(i.x>0 && i.x<widthMatrix && i.y-1<heightMatrix && i.y-1>0)
      matrix[i.y-1][i.x]='*';
    if(i.x+1>0 && i.x+1<widthMatrix && i.y-1<heightMatrix && i.y-1>0)
      matrix[i.y-1][i.x+1]='*';
    if(i.x-1>0 && i.x-1<widthMatrix && i.y<heightMatrix && i.y>0)
      matrix[i.y][i.x-1]='*';
    if(i.x>0 && i.x<widthMatrix && i.y<heightMatrix && i.y>0)
      matrix[i.y][i.x]='*';
    if(i.x+1>0 && i.x+1<widthMatrix && i.y<heightMatrix && i.y>0)
      matrix[i.y][i.x+1]='*';
    if(i.x>0 && i.x<widthMatrix && i.y+1<heightMatrix && i.y+1>0)
      matrix[i.y+1][i.x]='*';
  }
    if(goodGuy.x-1>0 && goodGuy.x-1<widthMatrix && goodGuy.y<heightMatrix && goodGuy.y>0)
      matrix[goodGuy.y][goodGuy.x-1]='*';
    if(goodGuy.x>0 && goodGuy.x<widthMatrix && goodGuy.y<heightMatrix && goodGuy.y>0)
      matrix[goodGuy.y][goodGuy.x]='*';
    if(goodGuy.x+1>0 && goodGuy.x+1<widthMatrix && goodGuy.y<heightMatrix && goodGuy.y>0)
      matrix[goodGuy.y][goodGuy.x+1]='*';
 
    for(auto i:shots)
      matrix[i.y][i.x]='|';
     
  for(int i=0; i<heightMatrix; i++){
    for(int j=0; j<widthMatrix; j++)
      cout<<matrix[i][j];
    cout<<endl;
  } 
}

void input(){
  char c=getch();
  lock_guard<mutex> inputGuard(inputMutex);
  switch(c){
    case 'a':
      if(goodGuy.x>2)
        goodGuy.x--;
      break;
    case 'd':
      if(goodGuy.x<widthMatrix-3)
        goodGuy.x++;
      break;
    case 27:
      gameOver = true;
      break;
    case 's':
      Shot s;
      s.x=goodGuy.x;
      s.y=goodGuy.y-1;
      shots.push_back(s);
      break;
  }
}

void inputLoop(){
  while(!gameOver){
    input();
  }
}

void update(){
  static int timePassed = 0;
  updateShot();
  if(timePassed>=55-5*level){
    updateEnemies();
    timePassed=0;
  }
  timePassed++;
  static bool levelChange = false;
  if(kills%10==0 && levelChange && kills!=0){
    level++;
    levelChange=false;
  }
  else if(kills%11==0)
    levelChange=true;
  
}

void updateEnemies(){
  generateEnemies();
  for(int i=0; i<enemies.size(); i++){
    enemies[i].y++;
    if(enemies[i].y==heightMatrix-3){
      gameOver=true;
      break;
    }
  }
}

void updateShot(){
  for(int i=0; i<shots.size(); i++){
    shots[i].y--;
    if(shots[i].y==0)
      shots.erase(shots.begin()+i);
    else
      for(int j=0; j<enemies.size(); j++){
        if(shots[i].y == enemies[j].y && (shots[i].x <= enemies[j].x+1 && shots[i].x >= enemies[j].x-1)){
          shots.erase(shots.begin()+i);
          enemies.erase(enemies.begin()+j);
          kills++;
        }
      }
  }
}
